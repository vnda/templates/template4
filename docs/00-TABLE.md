
<!-- _class: table-of-contents -->

# Orientações de cadastro

![Logo Vnda](../images/prints/vnda.svg)

## [Tabela de conteúdos](#1)

- ### [GERAL](#2)    - [1 POPUP DE NEWSLETTER](#2)    - [2 CARRINHO - FRETE GRÁTIS](#2)    - [3 SUGESTÕES CARRINHO](#2)    - [4 BANNER FAIXA TOPO](#2)    - [5 LOGO PRINCIPAL](#2)    - [6 MENU PRINCIPAL](#2)    - [7 BANNER DO SUBMENU](#2)    - [8 NEWSLETTER](#2)    - [9 RODAPÉ LOGO](#2)    - [10 RODAPÉ ATENDIMENTO](#2)    - [11 REDES SOCIAIS](#2)    - [12 MENU FOOTER](#2)    - [13 ASSINATURA - CNPJ](#2) - ### [HOME](#3)    - [1 FULLBANNER PRINCIPAL](#3)    - [2 GRID BANNERS DE CATEGORIA](#3)    - [3 TABS DE CATEGORIAS](#3)    - [4 TITULO TABS DE CATEGORIAS](#3)    - [5 MARCAS - CARROSSEL](#3)    - [6 BANNERS ESPELHADOS (até 6)](#3) - ### [TAG](#4)    - [1 TAG FULLBANNER](#4)    - [2 SLIDER DE CATEGORIAS](#4)    - [3 FILTRO](#4)    - [4 TAG FLAG](#4) - ### [PRODUTO](#5)    - [1 IMAGENS](#5)    - [2 DESCRIÇÃO](#5)    - [3 VARIANTES](#5)    - [4 TAG MODELO](#5)    - [5 GUIA DE MEDIDAS](#5)    - [6 TAG BANNER](#5)    - [7 BANNERS DE PRODUTO](#5)    - [8 SEÇÃO COMPRE JUNTO (até 3 produtos)](#5)    - [9 PRODUTOS RELACIONADOS](#5) - ### [SOBRE](#6)    - [1 FULLBANNER TOPO](#6)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#6)    - [3 BANNER IMAGEM E TEXTO](#6)    - [4 BANNER HORIZONTAL](#6)    - [5 BANNERS DEPOIMENTO](#6) - ### [TERMOS DE USO](#7)    - [1 ABAS LATERAIS](#7) - ### [ATENDIMENTO](#8)    - [1 FORMULÁRIO DE CONTATO](#8)    - [2 TEXTO FORMULÁRIO](#8)    - [3 BANNER DE INFORMAÇÕES DA LOJA (até 4)](#8)    - [4 SEÇÃO DE ABAS](#8)    - [5 SEÇÃO DE ABAS - TÍTULO](#8) - ### [ONDE-ENCONTRAR](#9)    - [1 FULLBANNER TOPO](#9)    - [2 LOCAIS](#9) 

***
