- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/02-home.png)

# HOME

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |


&nbsp;

## FULLBANNER PRINCIPAL

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Banner principal abaixo do header, ocupa 100% da largura da tela |
| **Cadastro exemplo**  | [Admin](https://template4.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                            |
| ------------------- | ------------------- | --------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x1012 pixels, Mobile: 1000x1460 pixels |
| **Título**          | :black_circle:      | Alt da imagem                                                         |
| **Subtítulo**       | :large_blue_circle: | Título do botão e posição do texto do banner. Opções a baixo!         |
| **Descrição**       | :large_blue_circle: | Título e descrição do banner                                          |
| **Externo?**        | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba                |
| **URL**             | :large_blue_circle: | Link de direcionamento                                                |
| **Posição Desktop** | :black_circle:      | `home-banner-principal`                                        |
| **Posição Mobile**  | :black_circle:      | `home-banner-principal-mobile`                                 |
| **Cor**             | :large_blue_circle: |  Cor dos textos                                                       |

&nbsp;

**_Exemplo de subtítulo:_**

```md
CALL TO ACTION | left-center
```

**_Exemplo de descrição:_**

```md
\#\#\# UPPER TITLE
\#\# Título do Banner

Descrição do banner alinhado à esquerda.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

**_Posições do botão possíveis para colocar no campo subtítulo:_**

- left-top
- left-center
- left-bottom

- center-top
- center-center
- center-bottom

- right-top
- right-center
- right-bottom


&nbsp;

## GRID BANNERS DE CATEGORIA

***Informações:***

| Dúvida                          | Instrução                                                       |
| ------------------------------- | --------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                         |
| **Onde será exibido**           | Grid de banners de categoria                  |
| **Cadastro exemplo em staging** | [Admin](https://template4.vnda.com.br/admin/midias/editar?id=5) |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                             |
| ------------- | ------------------- | ------------------------------------------------------ |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x750 pixels                      |
| **Título**    | :black_circle:      | Alt da imagem                                          |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se o banner possuir link                                                       |
| **Descrição** | :large_blue_circle: | Texto em Mardown, exemplo abaixo                                       |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba |
| **URL**       | :large_blue_circle: | Link de direcionamento                                 |
| **Posição**   | :black_circle:      | `home-banner-categoria`                                |
| **Cor**       | :no_entry:          |                           |

&nbsp;

**_Exemplo de descrição:_**

```md
\#\# Vestidos

Lorem ipsum dolor sit amet conse ctetur. Neque aliqu am natoque semper sit tempus fames suspe ndisse.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

&nbsp;

## TABS DE CATEGORIAS

***Informações:***

| Dúvida                          | Instrução                                              |
| ------------------------------- | ------------------------------------------------------ |
| **Onde cadastrar**              | Menu                                                   |
| **Onde será exibido**           | Tabs de categoria da home                              |
| **Níveis**                      | 1 nível                                                |
| **Cadastro exemplo em staging** | [Admin](https://template4.vnda.com.br/admin/navegacao) |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?     | Orientação                                 |
| ------------- | -------------- | ------------------------------------------ |
| **Título**    | :black_circle: | Título da tab                              |
| **Tooltip**   | :no_entry:     |                                            |
| **Descrição** | :no_entry:     |                                            |
| **Posição**   | :black_circle: | `home-produtos`                            |
| **Externo?**  | :no_entry:     |                                            |
| **URL**       | :black_circle: | Selecionar a tag que irá puxar os produtos |

&nbsp;

## TITULO TABS DE CATEGORIAS

***Informações:***

| Dúvida                          | Instrução                                                       |
| ------------------------------- | --------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                         |
| **Onde será exibido**           | Título da seção de tabs                                         |
| **Cadastro exemplo em staging** | [Admin](https://template4.vnda.com.br/admin/midias/editar?id=4) |

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação           |
| ------------- | ------------------- | -------------------- |
| **Imagem**    | :no_entry:          |                      |
| **Título**    | :black_circle:      | Não exibido no front |
| **Subtítulo** | :large_blue_circle: | Título da seção      |
| **Descrição** | :no_entry:          |                      |
| **Externo?**  | :no_entry:          |                      |
| **URL**       | :no_entry:          |                      |
| **Posição**   | :black_circle:      | `home-titulo-tabs`   |
| **Cor**       | :no_entry:          |                      |

## MARCAS - CARROSSEL

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Cadastro exemplo**  | [Admin](https://template4.vnda.dev) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                             |
| ------------- | ------------------- | ------------------------------------------------------ |
| **Imagem**    | :black_circle:      | Dimensões sugeridas 200 pixels de largura x altura livre. Importante não ultrapassar de 200x400 pixels |
| **Título**    | :black_circle:      | Alt da imagem                                          |
| **Subtítulo** | :no_entry:          |                                                        |
| **Descrição** | :no_entry:          |                                                        |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba |
| **URL**       | :large_blue_circle: | Link de direcionamento                                 |
| **Posição**   | :black_circle:      | `home-marcas`                                          |
| **Cor**       | :no_entry:          |                                                        |


&nbsp;
&nbsp;

## BANNERS ESPELHADOS (até 6)

***Informações:***

| Dúvida                          | Instrução                                                         |
| ------------------------------- | ----------------------------------------------------------------- |
| **Onde cadastrar**              | Banners                                                           |
| **Onde será exibido**           | Banners espelhados, abaixo dos banners de linha                   |
| **Cadastro exemplo em staging** | [Admin](https://template4.vnda.dev/)                          |

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                                |
| ------------- | ------------------- | --------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 1000x625 pixels                         |
| **Título**    | :black_circle:      | Alt da imagem                                             |
| **Subtítulo** | :large_blue_circle: | Texto do botão. Só será exibido se uma url for cadastrada |
| **Descrição** | :large_blue_circle: | Texto do banner. Exemplo abaixo                           |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba    |
| **URL**       | :large_blue_circle: | Link de direcionamento                                    |
| **Posição**   | :black_circle:      | `home-banner-espelhado`                            |
| **Cor**       | :no_entry:          |                                                           |

**_Exemplo de descrição:_**

```md
\#\# TÍTULO DA SEÇÃO

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
```

**OBSERVAÇÃO**: Obrigatório remover as barras antes dos jogos da velha (hashtag).

***
