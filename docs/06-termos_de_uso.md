
- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/06-termos_de_uso.png)

# TERMOS DE USO

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                                             |
| ---------------------------- | --------------------------------------------------------------------- |
| **Onde cadastrar**           | Páginas                                                               |
| **Onde será exibido**        | Página de termos-de-uso da loja                                        |
| **Cadastro exemplo**         | [Admin](https://template4.vnda.dev/admin/paginas/editar?id=termos-de-uso) |
| **Página para visualização** | [Página](https://template4.vnda.dev/p/termos-de-uso)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?     | Orientação                                          |
| ------------- | -------------- | --------------------------------------------------- |
| **Titulo**    | :black_circle: | Titulo da página                                    |
| **Url**       | :black_circle: | "termos-de-uso"                                      |
| **Descrição** | :black_circle: | Descrição da meta tag. Utilizada para melhorar SEO. |
| **Descrição** | :black_circle: | "."                                                 |

&nbsp;

## ABAS LATERAIS

**_Informações:_**

| Dúvida                | Instrução                                                     |
| --------------------- | ------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                       |
| **Onde será exibido** | Seção de abas laterais                                        |
| **Cadastro exemplo**  | [Admin](https://template4.vnda.dev/)                      |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                          |
| ------------- | ------------------- | ----------------------------------- |
| **Imagem**    | :no_entry:          |                                     |
| **Título**    | :black_circle:      | Título do banner. Apenas para controle interno                       |
| **Subtítulo** | :black_circle:      | Título da aba                       |
| **Descrição** | :black_circle:      | Conteúdo da aba. Aceita Markdown    |
| **Externo?**  | :no_entry:          |                                     |
| **URL**       | :large_blue_circle: | Id da aba. Ex.: "termos-de-uso", "politicas-dado". O id servirá para vincular o botão ao conteúdo textual e também como link para menus em rodapé etc |
| **Posição**   | :black_circle:      | `termos-de-uso-abas`                 |
| **Cor**       | :no_entry:          |                                     |

***
