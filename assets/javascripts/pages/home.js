import setFullbanner from './home/1_fullbanner';
import setSliderGrids from './home/4_categories';
import CategoryTabs from './home/3_category_tabs';
import setBrandsCarousel from './home/brands_carousel';

//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setSliderGrids();
		CategoryTabs.init();
		setBrandsCarousel();
		
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
