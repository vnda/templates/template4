import Swiper from 'swiper';
import { Pagination, Grid } from 'swiper/modules';

export default function setSliderGrids() {
  const sections = document.querySelectorAll('[data-slider-grid]');

  sections.forEach((section) => {
    const carousel = section.querySelector('.swiper');
    const pagination = section.querySelector('.swiper-pagination');

    if (carousel) {
      new Swiper(carousel, {
        modules: [Pagination, Grid],
        slidesPerView: 1,
        spaceBetween: 10,
        watchOverflow: true,
        speed: 1000,
        pagination: {
          el: pagination,
          type: 'bullets',
          clickable: true,
        },
        breakpoints: {
          768: {
            slidesPerView: 2,
            spaceBetween: 24,
            grid: {
              rows: 2,
              fill: 'row',
            },
          },
        },
      });
    }
  });
}
