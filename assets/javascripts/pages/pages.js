
import setTabs from './pages/abas';
import setFaq from './pages/faq';

//addImports

import setTopBar from "../components/topBar";

const Page = {
  init: function () {
    var _this = this;

    setTopBar();

    
		setTabs();
		setFaq()
		
		//calls

  },
};

window.addEventListener('DOMContentLoaded', () => {
  Page.init()
})
